# Integration of docvault and Task Manager

This documentation will walk-through on how to integrate docvault and task manager application. Please follow the instruction step by step:

**************************
__NOTE__:
All **words** with **CAPITAL LETTER** in **headers** , **parameters** and **ENDPOINT** enclosed with <> or {} denotes the **variables**

# 1. Get or Create TaskManager user

```python
params = {
    "email":"<EMAIL_ADDRESS>"
}
```

## ENDPOINT

```python
POST "/api/users/task-manager"
```

## EXAMPLE

### CURL

```shell
curl -X POST "http://dataroom.local/api/users/task-manager" -H  "accept: application/json" -H  "Content-Type: multipart/form-data" -F "email=om3ohf+38padf1iqfjkg@sharklasers.com"
```

### Response

```json
{
  "success": true,
  "message": "",
  "status_code": 200,
  "metadata": {},
  "data": {
    "id": 90,
    "name": "Task Manager",
    "email": "om3ohf+38padf1iqfjkg@sharklasers.com",
    "status": "active",
    "created_at": "2021-06-03 14:49:41.681488+05:45",
    "role": {
      "name": "administrator",
      "slug": "administrator"
    }
  }
}
```

# 2. Authorization

```python
params = {
    "client_id": "<CLIENT_ID>",
    "client_secret":"<CLIENT_SECRET>",
    "realms":"docvault",
    "identity":"<EMAIL_ADDRESS>"
}
```

## ENDPOINT

```shell
POST "/oauth/authenticate"
```

## EXAMPLE

### CURL

```shell
curl -X POST "http://dataroom.local/api/oauth/authenticate" -H "accept: application/json" -H "Content-Type: application/x-www-form-urlencoded" -d "client_id=taskmanager.dev&client_secret=caf2e452-d538-4464-b6c7-c8a3726ab58d&realms=docvault&identity=olisp1%2B39vnjpolplsks%40sharklasers.com"
```

### response

```json
{
  "success": true,
  "message": "",
  "status_code": 200,
  "metadata": {},
  "data": {
    "access_token": "eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJvcmlnaW5hbF90b2tlbiI6ImV5SmhiR2NpT2lKU1V6STFOaUlzSW5SNWNDSWdPaUFpU2xkVUlpd2lhMmxrSWlBNklDSmllWFptWkZVMlRrTXpkbkIyWVhOWmQxOXViWEpXTUdGVlZGbGZUMWt6VUdsS1JVc3lla1pqVnkxWkluMC5leUpsZUhBaU9qRTJNakkzTWprek16WXNJbWxoZENJNk1UWXlNalk1TXpNek5pd2lhblJwSWpvaU1XSTFNVE14TkdRdE9XSTBNQzAwWXpBMExXRTRPRE10WW1FeE1USXdaVGczTkdGaElpd2lhWE56SWpvaWFIUjBjSE02THk5a2IyTjJZWFZzZEM1amNtVmpaVzUwY21sakxtTnZiUzloZFhSb0wzSmxZV3h0Y3k5a2IyTjJZWFZzZENJc0ltRjFaQ0k2V3lKamNtVmpaVzUwY21sakxUWXdPV0l4WVdVM0xUQXlOVGd0TkRReE5pMDVNbUkxTFdFM09UWTVZbVUyWWpjMk9TSXNJbUZqWTI5MWJuUWlYU3dpYzNWaUlqb2lObVZpWmpkbVptWXROelF4WWkwME5UTTFMV0UwWmpRdE16UmhZak5tWlRGak56TTVJaXdpZEhsd0lqb2lRbVZoY21WeUlpd2lZWHB3SWpvaWRHRnphMjFoYm1GblpYSXVaR1YySWl3aVlXTnlJam9pTVNJc0ltRnNiRzkzWldRdGIzSnBaMmx1Y3lJNld5Sm9kSFJ3T2k4dmRHRnphMjFoYm1GblpYSXVaR1YySWwwc0luSmxZV3h0WDJGalkyVnpjeUk2ZXlKeWIyeGxjeUk2V3lKdlptWnNhVzVsWDJGalkyVnpjeUlzSW5WdFlWOWhkWFJvYjNKcGVtRjBhVzl1SWwxOUxDSnlaWE52ZFhKalpWOWhZMk5sYzNNaU9uc2lZV05qYjNWdWRDSTZleUp5YjJ4bGN5STZXeUp0WVc1aFoyVXRZV05qYjNWdWRDSXNJbTFoYm1GblpTMWhZMk52ZFc1MExXeHBibXR6SWl3aWRtbGxkeTF3Y205bWFXeGxJbDE5ZlN3aWMyTnZjR1VpT2lKdFpXUnBZVjkwWVdjNlpHVnNaWFJsSUcxbFpHbGhYM1JoWnpweVpXRmtJR1Z0WVdsc0lIQnliMnBsWTNRNmNtVmhaQ0J3Y205cVpXTjBYM1JoWnpwa1pXeGxkR1VnZEdGbmN6cHlaV0ZrSUhCeWIycGxZM1JmZEdGbk9uSmxZV1FnY0hKdmFtVmpkRHAzY21sMFpTQjBZV2R6T25keWFYUmxJRzFsWkdsaFgzUmhaenAzY21sMFpTQnRaV1JwWVRwM2NtbDBaU0J3Y205cVpXTjBYM1JoWnpwM2NtbDBaU0IxYzJWeU9uZHlhWFJsSUhWelpYSTZjbVZoWkNCd2NtOW1hV3hsSUhCeWIycGxZM1JmZEdGbk9tVmthWFFnYldWa2FXRmZkR0ZuT21Wa2FYUWdkR0ZuY3pwbFpHbDBJRzFsWkdsaE9uSmxZV1FpTENKbGJXRnBiRjkyWlhKcFptbGxaQ0k2Wm1Gc2MyVXNJbU5zYVdWdWRFaHZjM1FpT2lJME15NHlORFV1T0RZdU1UTWlMQ0pqYkdsbGJuUkpaQ0k2SW5SaGMydHRZVzVoWjJWeUxtUmxkaUlzSW5CeVpXWmxjbkpsWkY5MWMyVnlibUZ0WlNJNkluTmxjblpwWTJVdFlXTmpiM1Z1ZEMxMFlYTnJiV0Z1WVdkbGNpNWtaWFlpTENKamJHbGxiblJCWkdSeVpYTnpJam9pTkRNdU1qUTFMamcyTGpFekluMC5vTEY3Z3JrTlBxeDd4b21sajRscDlyVGR3QWRRZFFndkp6c0pfM01LeE5aN1B0RVRiR1JOb1laemp6eUp4Z09rR3l4R003azF1Znc0bG1Mb2xIVnhNZk5zYmowQzhQRWRwbEZxYnZINFluQTJNaE9PUGd3THRpY0dySVliQ2NJYjY2SGNydkxqSy1EWkc2VFNfamJWdDFkSVZOZ01jUjZOaE40d0o1SW1iYUstUVA3UEoxNDFwMURPZXc3eGJ3RmJZOU9KVC1sQUtpTDJqaDVtTGxCYmRHbW5MVXZ2NGhGRVgyNTktRHlJenVBaDdNUV9vbHZDM3RNdDlXR29VMzVVdFpqRVdOVXZJRU5nU0Q4aVNHOUlHTjd4OTB2ZzRsbm9VcmM0OUN6NXVyc2xaSksyZzJ4blBqOVV2TXdjQWxkRHJfbjlfNDkwNXkyUWRlZ014UWk5ckEiLCJpZGVudGl0eSI6Im9saXNwMSszOXZuanBvbHBsc2tzQHNoYXJrbGFzZXJzLmNvbSJ9.PMvMWVjkmQhubc5powMk3eJr3RmEileaJt8LmXy9wfc",
    "expires_in": 36000,
    "refresh_expires_in": 0,
    "token_type": "Bearer",
    "not-before-policy": 1622006716,
    "scope": "media_tag:delete media_tag:read email project:read project_tag:delete tags:read project_tag:read project:write tags:write media_tag:write media:write project_tag:write user:write user:read profile project_tag:edit media_tag:edit tags:edit media:read"
  }
}
```

# 3. get, map or create project

## STEP 1: search if project exists or not

```python
headers = {
    "Authorization": "<TOKEN>",
    "x-token-type":"Access",
}
params = {
    "tags":"<KEY_VALUE_PAIR>" # object. Valid key: ["deal_uuid"]. eg. {"deal_uuid": "870d052d-9d10-45c2-8245-ba92c6e4d66a"}
}
```

### ENDPOINT

```shell
POST "/api/projects"
```

### EXAMPLE

#### CURL

```shell
curl -X POST "http://dataroom.local/api/projects" -H  "accept: application/json" -H  "Authorization: eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJvcmlnaW5hbF90b2tlbiI6ImV5SmhiR2NpT2lKU1V6STFOaUlzSW5SNWNDSWdPaUFpU2xkVUlpd2lhMmxrSWlBNklDSmllWFptWkZVMlRrTXpkbkIyWVhOWmQxOXViWEpXTUdGVlZGbGZUMWt6VUdsS1JVc3lla1pqVnkxWkluMC5leUpsZUhBaU9qRTJNakkzTXpBek1EQXNJbWxoZENJNk1UWXlNalk1TkRNd01Dd2lhblJwSWpvaU56Sm1NRGMzTWpNdFl6STBNUzAwTW1VNExXSTVaRGt0Tm1NMk5tTm1aV1F6TVdaaElpd2lhWE56SWpvaWFIUjBjSE02THk5a2IyTjJZWFZzZEM1amNtVmpaVzUwY21sakxtTnZiUzloZFhSb0wzSmxZV3h0Y3k5a2IyTjJZWFZzZENJc0ltRjFaQ0k2V3lKamNtVmpaVzUwY21sakxUWXdPV0l4WVdVM0xUQXlOVGd0TkRReE5pMDVNbUkxTFdFM09UWTVZbVUyWWpjMk9TSXNJbUZqWTI5MWJuUWlYU3dpYzNWaUlqb2lObVZpWmpkbVptWXROelF4WWkwME5UTTFMV0UwWmpRdE16UmhZak5tWlRGak56TTVJaXdpZEhsd0lqb2lRbVZoY21WeUlpd2lZWHB3SWpvaWRHRnphMjFoYm1GblpYSXVaR1YySWl3aVlXTnlJam9pTVNJc0ltRnNiRzkzWldRdGIzSnBaMmx1Y3lJNld5Sm9kSFJ3T2k4dmRHRnphMjFoYm1GblpYSXVaR1YySWwwc0luSmxZV3h0WDJGalkyVnpjeUk2ZXlKeWIyeGxjeUk2V3lKdlptWnNhVzVsWDJGalkyVnpjeUlzSW5WdFlWOWhkWFJvYjNKcGVtRjBhVzl1SWwxOUxDSnlaWE52ZFhKalpWOWhZMk5sYzNNaU9uc2lZV05qYjNWdWRDSTZleUp5YjJ4bGN5STZXeUp0WVc1aFoyVXRZV05qYjNWdWRDSXNJbTFoYm1GblpTMWhZMk52ZFc1MExXeHBibXR6SWl3aWRtbGxkeTF3Y205bWFXeGxJbDE5ZlN3aWMyTnZjR1VpT2lKdFpXUnBZVjkwWVdjNlpHVnNaWFJsSUcxbFpHbGhYM1JoWnpweVpXRmtJR1Z0WVdsc0lIQnliMnBsWTNRNmNtVmhaQ0J3Y205cVpXTjBYM1JoWnpwa1pXeGxkR1VnZEdGbmN6cHlaV0ZrSUhCeWIycGxZM1JmZEdGbk9uSmxZV1FnY0hKdmFtVmpkRHAzY21sMFpTQjBZV2R6T25keWFYUmxJRzFsWkdsaFgzUmhaenAzY21sMFpTQnRaV1JwWVRwM2NtbDBaU0J3Y205cVpXTjBYM1JoWnpwM2NtbDBaU0IxYzJWeU9uZHlhWFJsSUhWelpYSTZjbVZoWkNCd2NtOW1hV3hsSUhCeWIycGxZM1JmZEdGbk9tVmthWFFnYldWa2FXRmZkR0ZuT21Wa2FYUWdkR0ZuY3pwbFpHbDBJRzFsWkdsaE9uSmxZV1FpTENKbGJXRnBiRjkyWlhKcFptbGxaQ0k2Wm1Gc2MyVXNJbU5zYVdWdWRFaHZjM1FpT2lJME15NHlORFV1T0RZdU1UTWlMQ0pqYkdsbGJuUkpaQ0k2SW5SaGMydHRZVzVoWjJWeUxtUmxkaUlzSW5CeVpXWmxjbkpsWkY5MWMyVnlibUZ0WlNJNkluTmxjblpwWTJVdFlXTmpiM1Z1ZEMxMFlYTnJiV0Z1WVdkbGNpNWtaWFlpTENKamJHbGxiblJCWkdSeVpYTnpJam9pTkRNdU1qUTFMamcyTGpFekluMC5mQXZjWWhVclNSemw4dWNfNV9qWlRSbDRieUJGOElhQnd4NEpWNl92dF9ObmlLelluNXpqcERWeUlOYW5jVnJaV3lTTlh0eUhycXNXeFlTRWFCWWdfcGx5RVI2RDJsUXlueXVESXdianFrTm56c1BXMmxkd3hHXzdjM0EtU3AtX3BLNHE5alJiYkhpTUNaSnhNNVdOWEU5QzJVZmZXUTRKd3hBN0JzaDZ6QlhjYUdpTDdFZmRTeDFPNzdqdGE4SUo5N3dXMHdTejdXUWtsZXFnYWFmX2lnQlo1QlhrZkdKaWlXNkFSYnRwdElUaUpZajhaZTZzQjlvNnlYUDBONnJpU1RtbXBBQ2NaYzg4WXJKaFBEYVpDSU56ODFkT01ZdW9rTmt6Y3ZzRXUxWUVHbk9sUHBUS3J5eERuR3FKSUVLVTJ5MkJ2RDFNYWhQWGZoMUpKb2h0MVEiLCJpZGVudGl0eSI6Im9saXNwMSszOXZuanBvbHBsc2tzQHNoYXJrbGFzZXJzLmNvbSJ9.Ab8VVYs63iuGlenhuNc38WCiQBqFDdeTGXE1CsLvtgQ" -H  "x-token-type: Access" -H  "Content-Type: application/x-www-form-urlencoded" -d "tags=%7B%22deal_uuid%22%3A%22cc5fc3f4-c2xa-11eb-8529-0242ac130103%22%7D"
```

#### Response

```json
{
  "success": true,
  "message": "",
  "status_code": 200,
  "metadata": {},
  "data": [
    {
      "id": 1659,
      "name": "x1211xx1",
      "slug": "x1211xx1_a63c147c-09cd-4294-a40e-9b491ad17a49",
      "status": "inbox",
      "created_at": "2021-06-02 15:00:17.551223+05:45",
      "owner": "ABC user",
      "owner_id": 18,
      "shared": false,
      "tag_detail": {
        "application_name": "taskmanager",
        "deal_uuid": "cc5fc3f4-c2ca-11eb-8529-0242ac130103"
      }
    }
  ]
}
```

## STEP 2: If no project is found then, we have two options

### a. create new project and map with tags

### b. select existing project and map with tags

### option 1: create new project and tag it

```python
headers = {
    "Authorization": "<TOKEN>",
    "x-token-type":"Access",
}
params = {
    "name":"<PROJECT_NAME>",
    "tags":"<KEY_VALUE_PAIR>" # object. Valid key: ["deal_uuid"]. eg. {"deal_uuid": "870d052d-9d10-45c2-8245-ba92c6e4d66a"}
}
```

#### ENDPOINT

```shell
POST "/api/projects"
```

#### EXAMPLE

##### CURL

```shell
curl -X POST "http://dataroom.local/api/projects" -H  "accept: application/json" -H  "Authorization: eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJvcmlnaW5hbF90b2tlbiI6ImV5SmhiR2NpT2lKU1V6STFOaUlzSW5SNWNDSWdPaUFpU2xkVUlpd2lhMmxrSWlBNklDSmllWFptWkZVMlRrTXpkbkIyWVhOWmQxOXViWEpXTUdGVlZGbGZUMWt6VUdsS1JVc3lla1pqVnkxWkluMC5leUpsZUhBaU9qRTJNakkzTXpBek1EQXNJbWxoZENJNk1UWXlNalk1TkRNd01Dd2lhblJwSWpvaU56Sm1NRGMzTWpNdFl6STBNUzAwTW1VNExXSTVaRGt0Tm1NMk5tTm1aV1F6TVdaaElpd2lhWE56SWpvaWFIUjBjSE02THk5a2IyTjJZWFZzZEM1amNtVmpaVzUwY21sakxtTnZiUzloZFhSb0wzSmxZV3h0Y3k5a2IyTjJZWFZzZENJc0ltRjFaQ0k2V3lKamNtVmpaVzUwY21sakxUWXdPV0l4WVdVM0xUQXlOVGd0TkRReE5pMDVNbUkxTFdFM09UWTVZbVUyWWpjMk9TSXNJbUZqWTI5MWJuUWlYU3dpYzNWaUlqb2lObVZpWmpkbVptWXROelF4WWkwME5UTTFMV0UwWmpRdE16UmhZak5tWlRGak56TTVJaXdpZEhsd0lqb2lRbVZoY21WeUlpd2lZWHB3SWpvaWRHRnphMjFoYm1GblpYSXVaR1YySWl3aVlXTnlJam9pTVNJc0ltRnNiRzkzWldRdGIzSnBaMmx1Y3lJNld5Sm9kSFJ3T2k4dmRHRnphMjFoYm1GblpYSXVaR1YySWwwc0luSmxZV3h0WDJGalkyVnpjeUk2ZXlKeWIyeGxjeUk2V3lKdlptWnNhVzVsWDJGalkyVnpjeUlzSW5WdFlWOWhkWFJvYjNKcGVtRjBhVzl1SWwxOUxDSnlaWE52ZFhKalpWOWhZMk5sYzNNaU9uc2lZV05qYjNWdWRDSTZleUp5YjJ4bGN5STZXeUp0WVc1aFoyVXRZV05qYjNWdWRDSXNJbTFoYm1GblpTMWhZMk52ZFc1MExXeHBibXR6SWl3aWRtbGxkeTF3Y205bWFXeGxJbDE5ZlN3aWMyTnZjR1VpT2lKdFpXUnBZVjkwWVdjNlpHVnNaWFJsSUcxbFpHbGhYM1JoWnpweVpXRmtJR1Z0WVdsc0lIQnliMnBsWTNRNmNtVmhaQ0J3Y205cVpXTjBYM1JoWnpwa1pXeGxkR1VnZEdGbmN6cHlaV0ZrSUhCeWIycGxZM1JmZEdGbk9uSmxZV1FnY0hKdmFtVmpkRHAzY21sMFpTQjBZV2R6T25keWFYUmxJRzFsWkdsaFgzUmhaenAzY21sMFpTQnRaV1JwWVRwM2NtbDBaU0J3Y205cVpXTjBYM1JoWnpwM2NtbDBaU0IxYzJWeU9uZHlhWFJsSUhWelpYSTZjbVZoWkNCd2NtOW1hV3hsSUhCeWIycGxZM1JmZEdGbk9tVmthWFFnYldWa2FXRmZkR0ZuT21Wa2FYUWdkR0ZuY3pwbFpHbDBJRzFsWkdsaE9uSmxZV1FpTENKbGJXRnBiRjkyWlhKcFptbGxaQ0k2Wm1Gc2MyVXNJbU5zYVdWdWRFaHZjM1FpT2lJME15NHlORFV1T0RZdU1UTWlMQ0pqYkdsbGJuUkpaQ0k2SW5SaGMydHRZVzVoWjJWeUxtUmxkaUlzSW5CeVpXWmxjbkpsWkY5MWMyVnlibUZ0WlNJNkluTmxjblpwWTJVdFlXTmpiM1Z1ZEMxMFlYTnJiV0Z1WVdkbGNpNWtaWFlpTENKamJHbGxiblJCWkdSeVpYTnpJam9pTkRNdU1qUTFMamcyTGpFekluMC5mQXZjWWhVclNSemw4dWNfNV9qWlRSbDRieUJGOElhQnd4NEpWNl92dF9ObmlLelluNXpqcERWeUlOYW5jVnJaV3lTTlh0eUhycXNXeFlTRWFCWWdfcGx5RVI2RDJsUXlueXVESXdianFrTm56c1BXMmxkd3hHXzdjM0EtU3AtX3BLNHE5alJiYkhpTUNaSnhNNVdOWEU5QzJVZmZXUTRKd3hBN0JzaDZ6QlhjYUdpTDdFZmRTeDFPNzdqdGE4SUo5N3dXMHdTejdXUWtsZXFnYWFmX2lnQlo1QlhrZkdKaWlXNkFSYnRwdElUaUpZajhaZTZzQjlvNnlYUDBONnJpU1RtbXBBQ2NaYzg4WXJKaFBEYVpDSU56ODFkT01ZdW9rTmt6Y3ZzRXUxWUVHbk9sUHBUS3J5eERuR3FKSUVLVTJ5MkJ2RDFNYWhQWGZoMUpKb2h0MVEiLCJpZGVudGl0eSI6Im9saXNwMSszOXZuanBvbHBsc2tzQHNoYXJrbGFzZXJzLmNvbSJ9.Ab8VVYs63iuGlenhuNc38WCiQBqFDdeTGXE1CsLvtgQ" -H  "x-token-type: Access" -H  "Content-Type: application/x-www-form-urlencoded" -d "name=x1212x&tags=%7B%22deal_uuid%22%3A%22cc5fc3f4-c2xa-11eb-8529-0242ac130103%22%7D"
```

##### Response

```json
{
  "success": true,
  "message": "",
  "status_code": 200,
  "metadata": {},
  "data": {
    "id": 1661,
    "name": "x1212x",
    "slug": "x1212x_8de73c39-f550-4a50-b023-c786ac2eaba9",
    "status": "inbox",
    "created_at": "2021-06-03 11:54:14.663174",
    "owner": "Task Manager",
    "owner_id": 89,
    "shared": false,
    "tag_detail": {
      "application_name": "taskmanager",
      "deal_uuid": "cc5fc3f4-c2xa-11eb-8529-0242ac130103"
    }
  }
}
```

## option 2: select existing project and tag it

### STEP 1: get all projects

- Lists all projects shared to me and all projects that is created by me.

**Note: If you are super-admin with manage permission then you will be listed with all the projects created by others too.**

```python
headers = {
    "Authorization": "<TOKEN>",
    "x-token-type":"Access",
}
params = {
    "page":"<PAGE_NO>",
}
```

#### ENDPOINT

```shell
GET "/api/projects/all-listing?page=1"
```

#### EXAMPLE 

##### CURL

```shell
curl -X GET "http://dataroom.local/api/projects/all-listing?page=1" -H  "accept: application/json" -H  "Authorization: eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJvcmlnaW5hbF90b2tlbiI6ImV5SmhiR2NpT2lKU1V6STFOaUlzSW5SNWNDSWdPaUFpU2xkVUlpd2lhMmxrSWlBNklDSmllWFptWkZVMlRrTXpkbkIyWVhOWmQxOXViWEpXTUdGVlZGbGZUMWt6VUdsS1JVc3lla1pqVnkxWkluMC5leUpsZUhBaU9qRTJNakkzTXpBek1EQXNJbWxoZENJNk1UWXlNalk1TkRNd01Dd2lhblJwSWpvaU56Sm1NRGMzTWpNdFl6STBNUzAwTW1VNExXSTVaRGt0Tm1NMk5tTm1aV1F6TVdaaElpd2lhWE56SWpvaWFIUjBjSE02THk5a2IyTjJZWFZzZEM1amNtVmpaVzUwY21sakxtTnZiUzloZFhSb0wzSmxZV3h0Y3k5a2IyTjJZWFZzZENJc0ltRjFaQ0k2V3lKamNtVmpaVzUwY21sakxUWXdPV0l4WVdVM0xUQXlOVGd0TkRReE5pMDVNbUkxTFdFM09UWTVZbVUyWWpjMk9TSXNJbUZqWTI5MWJuUWlYU3dpYzNWaUlqb2lObVZpWmpkbVptWXROelF4WWkwME5UTTFMV0UwWmpRdE16UmhZak5tWlRGak56TTVJaXdpZEhsd0lqb2lRbVZoY21WeUlpd2lZWHB3SWpvaWRHRnphMjFoYm1GblpYSXVaR1YySWl3aVlXTnlJam9pTVNJc0ltRnNiRzkzWldRdGIzSnBaMmx1Y3lJNld5Sm9kSFJ3T2k4dmRHRnphMjFoYm1GblpYSXVaR1YySWwwc0luSmxZV3h0WDJGalkyVnpjeUk2ZXlKeWIyeGxjeUk2V3lKdlptWnNhVzVsWDJGalkyVnpjeUlzSW5WdFlWOWhkWFJvYjNKcGVtRjBhVzl1SWwxOUxDSnlaWE52ZFhKalpWOWhZMk5sYzNNaU9uc2lZV05qYjNWdWRDSTZleUp5YjJ4bGN5STZXeUp0WVc1aFoyVXRZV05qYjNWdWRDSXNJbTFoYm1GblpTMWhZMk52ZFc1MExXeHBibXR6SWl3aWRtbGxkeTF3Y205bWFXeGxJbDE5ZlN3aWMyTnZjR1VpT2lKdFpXUnBZVjkwWVdjNlpHVnNaWFJsSUcxbFpHbGhYM1JoWnpweVpXRmtJR1Z0WVdsc0lIQnliMnBsWTNRNmNtVmhaQ0J3Y205cVpXTjBYM1JoWnpwa1pXeGxkR1VnZEdGbmN6cHlaV0ZrSUhCeWIycGxZM1JmZEdGbk9uSmxZV1FnY0hKdmFtVmpkRHAzY21sMFpTQjBZV2R6T25keWFYUmxJRzFsWkdsaFgzUmhaenAzY21sMFpTQnRaV1JwWVRwM2NtbDBaU0J3Y205cVpXTjBYM1JoWnpwM2NtbDBaU0IxYzJWeU9uZHlhWFJsSUhWelpYSTZjbVZoWkNCd2NtOW1hV3hsSUhCeWIycGxZM1JmZEdGbk9tVmthWFFnYldWa2FXRmZkR0ZuT21Wa2FYUWdkR0ZuY3pwbFpHbDBJRzFsWkdsaE9uSmxZV1FpTENKbGJXRnBiRjkyWlhKcFptbGxaQ0k2Wm1Gc2MyVXNJbU5zYVdWdWRFaHZjM1FpT2lJME15NHlORFV1T0RZdU1UTWlMQ0pqYkdsbGJuUkpaQ0k2SW5SaGMydHRZVzVoWjJWeUxtUmxkaUlzSW5CeVpXWmxjbkpsWkY5MWMyVnlibUZ0WlNJNkluTmxjblpwWTJVdFlXTmpiM1Z1ZEMxMFlYTnJiV0Z1WVdkbGNpNWtaWFlpTENKamJHbGxiblJCWkdSeVpYTnpJam9pTkRNdU1qUTFMamcyTGpFekluMC5mQXZjWWhVclNSemw4dWNfNV9qWlRSbDRieUJGOElhQnd4NEpWNl92dF9ObmlLelluNXpqcERWeUlOYW5jVnJaV3lTTlh0eUhycXNXeFlTRWFCWWdfcGx5RVI2RDJsUXlueXVESXdianFrTm56c1BXMmxkd3hHXzdjM0EtU3AtX3BLNHE5alJiYkhpTUNaSnhNNVdOWEU5QzJVZmZXUTRKd3hBN0JzaDZ6QlhjYUdpTDdFZmRTeDFPNzdqdGE4SUo5N3dXMHdTejdXUWtsZXFnYWFmX2lnQlo1QlhrZkdKaWlXNkFSYnRwdElUaUpZajhaZTZzQjlvNnlYUDBONnJpU1RtbXBBQ2NaYzg4WXJKaFBEYVpDSU56ODFkT01ZdW9rTmt6Y3ZzRXUxWUVHbk9sUHBUS3J5eERuR3FKSUVLVTJ5MkJ2RDFNYWhQWGZoMUpKb2h0MVEiLCJpZGVudGl0eSI6Im9saXNwMSszOXZuanBvbHBsc2tzQHNoYXJrbGFzZXJzLmNvbSJ9.Ab8VVYs63iuGlenhuNc38WCiQBqFDdeTGXE1CsLvtgQ" -H  "x-token-type: Access"
```

##### Response

```json
{
  "success": true,
  "message": "",
  "status_code": 200,
  "metadata": {
    "per_page": 10,
    "total_records": 144,
    "current_page": 1,
    "total_page": 15
  },
  "data": [
    {
      "id": 742,
      "name": "ramesh project 123",
      "slug": "ramesh_project_123_1ff6890c-4fc5-4f31-8b8d-6062330acba5",
      "reference": {
        "reference_id": "",
        "reference_slug": "",
        "reference_object": ""
      },
      "parent_id": null,
      "created_at": "2021-08-02 10:10:23.097187+05:45",
      "owner": "Ramesh Pradhan",
      "owner_id": 67,
      "permission": {
        "read": 1,
        "write": 1,
        "edit": 1,
        "delete": 1,
        "manage": 1
      },
      "shared": false,
      "starred": false,
      "breadcrumbs": [
        {
          "id": 742,
          "name": "ramesh project 123",
          "type": "project"
        }
      ],
      "tags_detail": {}
    },
    {
      "id": 741,
      "name": "project 29",
      "slug": "project_29_a151b58b-81a6-4fe4-be1a-b1a4aaa47ff1",
      "reference": {
        "reference_id": "",
        "reference_slug": "",
        "reference_object": ""
      },
      "parent_id": null,
      "created_at": "2021-07-29 09:36:33.151625+05:45",
      "owner": "Task Manager",
      "owner_id": 69,
      "permission": {
        "read": 1,
        "write": 1,
        "edit": 1,
        "delete": 1,
        "manage": 1
      },
      "shared": false,
      "starred": false,
      "breadcrumbs": [
        {
          "id": 741,
          "name": "project 29",
          "type": "project"
        }
      ],
      "tags_detail": {
        "application_name": "taskmanager",
        "deal_uuid": "995f5e06-79e2-430a-b961-1dce8edad1f5"
      }
    }
  ]
}
```

## STEP 2: Tag the project

### STEP 2.1: use project's id obtained from step 1 and tag the project with "deal_uuid"

```python
headers = {
    "Authorization": "<TOKEN>",
    "x-token-type":"Access",
}
params = {
    "project_id":"<PROJECT_ID>", # id obtained from step 1 eg. response.data[0].id
    "tag":"deal_uuid", 
    "value":"<UUID>" # id of deal
}
```

#### ENDPOINT

```shell
POST "/api/projects/{PROJECT_ID}/tags"
```

#### EXAMPLE

##### CURL

```shell
curl -X POST "http://dataroom.local/api/projects/1662/tags" -H  "accept: application/json" -H  "Authorization: eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJvcmlnaW5hbF90b2tlbiI6ImV5SmhiR2NpT2lKU1V6STFOaUlzSW5SNWNDSWdPaUFpU2xkVUlpd2lhMmxrSWlBNklDSmllWFptWkZVMlRrTXpkbkIyWVhOWmQxOXViWEpXTUdGVlZGbGZUMWt6VUdsS1JVc3lla1pqVnkxWkluMC5leUpsZUhBaU9qRTJNakkzTXpBek1EQXNJbWxoZENJNk1UWXlNalk1TkRNd01Dd2lhblJwSWpvaU56Sm1NRGMzTWpNdFl6STBNUzAwTW1VNExXSTVaRGt0Tm1NMk5tTm1aV1F6TVdaaElpd2lhWE56SWpvaWFIUjBjSE02THk5a2IyTjJZWFZzZEM1amNtVmpaVzUwY21sakxtTnZiUzloZFhSb0wzSmxZV3h0Y3k5a2IyTjJZWFZzZENJc0ltRjFaQ0k2V3lKamNtVmpaVzUwY21sakxUWXdPV0l4WVdVM0xUQXlOVGd0TkRReE5pMDVNbUkxTFdFM09UWTVZbVUyWWpjMk9TSXNJbUZqWTI5MWJuUWlYU3dpYzNWaUlqb2lObVZpWmpkbVptWXROelF4WWkwME5UTTFMV0UwWmpRdE16UmhZak5tWlRGak56TTVJaXdpZEhsd0lqb2lRbVZoY21WeUlpd2lZWHB3SWpvaWRHRnphMjFoYm1GblpYSXVaR1YySWl3aVlXTnlJam9pTVNJc0ltRnNiRzkzWldRdGIzSnBaMmx1Y3lJNld5Sm9kSFJ3T2k4dmRHRnphMjFoYm1GblpYSXVaR1YySWwwc0luSmxZV3h0WDJGalkyVnpjeUk2ZXlKeWIyeGxjeUk2V3lKdlptWnNhVzVsWDJGalkyVnpjeUlzSW5WdFlWOWhkWFJvYjNKcGVtRjBhVzl1SWwxOUxDSnlaWE52ZFhKalpWOWhZMk5sYzNNaU9uc2lZV05qYjNWdWRDSTZleUp5YjJ4bGN5STZXeUp0WVc1aFoyVXRZV05qYjNWdWRDSXNJbTFoYm1GblpTMWhZMk52ZFc1MExXeHBibXR6SWl3aWRtbGxkeTF3Y205bWFXeGxJbDE5ZlN3aWMyTnZjR1VpT2lKdFpXUnBZVjkwWVdjNlpHVnNaWFJsSUcxbFpHbGhYM1JoWnpweVpXRmtJR1Z0WVdsc0lIQnliMnBsWTNRNmNtVmhaQ0J3Y205cVpXTjBYM1JoWnpwa1pXeGxkR1VnZEdGbmN6cHlaV0ZrSUhCeWIycGxZM1JmZEdGbk9uSmxZV1FnY0hKdmFtVmpkRHAzY21sMFpTQjBZV2R6T25keWFYUmxJRzFsWkdsaFgzUmhaenAzY21sMFpTQnRaV1JwWVRwM2NtbDBaU0J3Y205cVpXTjBYM1JoWnpwM2NtbDBaU0IxYzJWeU9uZHlhWFJsSUhWelpYSTZjbVZoWkNCd2NtOW1hV3hsSUhCeWIycGxZM1JmZEdGbk9tVmthWFFnYldWa2FXRmZkR0ZuT21Wa2FYUWdkR0ZuY3pwbFpHbDBJRzFsWkdsaE9uSmxZV1FpTENKbGJXRnBiRjkyWlhKcFptbGxaQ0k2Wm1Gc2MyVXNJbU5zYVdWdWRFaHZjM1FpT2lJME15NHlORFV1T0RZdU1UTWlMQ0pqYkdsbGJuUkpaQ0k2SW5SaGMydHRZVzVoWjJWeUxtUmxkaUlzSW5CeVpXWmxjbkpsWkY5MWMyVnlibUZ0WlNJNkluTmxjblpwWTJVdFlXTmpiM1Z1ZEMxMFlYTnJiV0Z1WVdkbGNpNWtaWFlpTENKamJHbGxiblJCWkdSeVpYTnpJam9pTkRNdU1qUTFMamcyTGpFekluMC5mQXZjWWhVclNSemw4dWNfNV9qWlRSbDRieUJGOElhQnd4NEpWNl92dF9ObmlLelluNXpqcERWeUlOYW5jVnJaV3lTTlh0eUhycXNXeFlTRWFCWWdfcGx5RVI2RDJsUXlueXVESXdianFrTm56c1BXMmxkd3hHXzdjM0EtU3AtX3BLNHE5alJiYkhpTUNaSnhNNVdOWEU5QzJVZmZXUTRKd3hBN0JzaDZ6QlhjYUdpTDdFZmRTeDFPNzdqdGE4SUo5N3dXMHdTejdXUWtsZXFnYWFmX2lnQlo1QlhrZkdKaWlXNkFSYnRwdElUaUpZajhaZTZzQjlvNnlYUDBONnJpU1RtbXBBQ2NaYzg4WXJKaFBEYVpDSU56ODFkT01ZdW9rTmt6Y3ZzRXUxWUVHbk9sUHBUS3J5eERuR3FKSUVLVTJ5MkJ2RDFNYWhQWGZoMUpKb2h0MVEiLCJpZGVudGl0eSI6Im9saXNwMSszOXZuanBvbHBsc2tzQHNoYXJrbGFzZXJzLmNvbSJ9.Ab8VVYs63iuGlenhuNc38WCiQBqFDdeTGXE1CsLvtgQ" -H  "x-token-type: Access" -H  "Content-Type: application/x-www-form-urlencoded" -d "tag=deal_uuid&value=870d052d-9d10-45c2-8245-ba92c6e4d66a"
```

##### Response

```json
{
  "success": true,
  "message": "",
  "status_code": 200,
  "metadata": {},
  "data": {
    "id": 1414,
    "created_by": {
      "id": 89,
      "name": "Task Manager"
    },
    "updated_by": {
      "id": null,
      "name": null
    },
    "created_at": "2021-06-03 12:06:08.299270",
    "updated_at": null,
    "tag_detail": {
      "tag_id": 120,
      "tag_name": "application_name",
      "value": "870d052d-9d10-45c2-8245-ba92c6e4d66a"
    },
    "project_detail": {
      "project_id": "1662",
      "project_name": "x1212xyz"
    }
  }
}
```

### STEP 2.2: use project's id obtained from STEP 1 and tag the project with "application_name"

```python
headers = {
    "Authorization": "<TOKEN>",
    "x-token-type":"Access",
}
params = {
    "project_id":"<PROJECT_ID>", # id obtained from step 1 eg. response.data[0].id
    "tag":"application_name", 
    "value":"taskmanager" 
}
```

#### ENDPOINT

```shell
POST "/api/projects/{PROJECT_ID}/tags"
```

#### EXAMPLE

##### CURL

```shell
curl -X POST "http://dataroom.local/api/projects/1662/tags" -H  "accept: application/json" -H  "Authorization: eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJvcmlnaW5hbF90b2tlbiI6ImV5SmhiR2NpT2lKU1V6STFOaUlzSW5SNWNDSWdPaUFpU2xkVUlpd2lhMmxrSWlBNklDSmllWFptWkZVMlRrTXpkbkIyWVhOWmQxOXViWEpXTUdGVlZGbGZUMWt6VUdsS1JVc3lla1pqVnkxWkluMC5leUpsZUhBaU9qRTJNakkzTXpBek1EQXNJbWxoZENJNk1UWXlNalk1TkRNd01Dd2lhblJwSWpvaU56Sm1NRGMzTWpNdFl6STBNUzAwTW1VNExXSTVaRGt0Tm1NMk5tTm1aV1F6TVdaaElpd2lhWE56SWpvaWFIUjBjSE02THk5a2IyTjJZWFZzZEM1amNtVmpaVzUwY21sakxtTnZiUzloZFhSb0wzSmxZV3h0Y3k5a2IyTjJZWFZzZENJc0ltRjFaQ0k2V3lKamNtVmpaVzUwY21sakxUWXdPV0l4WVdVM0xUQXlOVGd0TkRReE5pMDVNbUkxTFdFM09UWTVZbVUyWWpjMk9TSXNJbUZqWTI5MWJuUWlYU3dpYzNWaUlqb2lObVZpWmpkbVptWXROelF4WWkwME5UTTFMV0UwWmpRdE16UmhZak5tWlRGak56TTVJaXdpZEhsd0lqb2lRbVZoY21WeUlpd2lZWHB3SWpvaWRHRnphMjFoYm1GblpYSXVaR1YySWl3aVlXTnlJam9pTVNJc0ltRnNiRzkzWldRdGIzSnBaMmx1Y3lJNld5Sm9kSFJ3T2k4dmRHRnphMjFoYm1GblpYSXVaR1YySWwwc0luSmxZV3h0WDJGalkyVnpjeUk2ZXlKeWIyeGxjeUk2V3lKdlptWnNhVzVsWDJGalkyVnpjeUlzSW5WdFlWOWhkWFJvYjNKcGVtRjBhVzl1SWwxOUxDSnlaWE52ZFhKalpWOWhZMk5sYzNNaU9uc2lZV05qYjNWdWRDSTZleUp5YjJ4bGN5STZXeUp0WVc1aFoyVXRZV05qYjNWdWRDSXNJbTFoYm1GblpTMWhZMk52ZFc1MExXeHBibXR6SWl3aWRtbGxkeTF3Y205bWFXeGxJbDE5ZlN3aWMyTnZjR1VpT2lKdFpXUnBZVjkwWVdjNlpHVnNaWFJsSUcxbFpHbGhYM1JoWnpweVpXRmtJR1Z0WVdsc0lIQnliMnBsWTNRNmNtVmhaQ0J3Y205cVpXTjBYM1JoWnpwa1pXeGxkR1VnZEdGbmN6cHlaV0ZrSUhCeWIycGxZM1JmZEdGbk9uSmxZV1FnY0hKdmFtVmpkRHAzY21sMFpTQjBZV2R6T25keWFYUmxJRzFsWkdsaFgzUmhaenAzY21sMFpTQnRaV1JwWVRwM2NtbDBaU0J3Y205cVpXTjBYM1JoWnpwM2NtbDBaU0IxYzJWeU9uZHlhWFJsSUhWelpYSTZjbVZoWkNCd2NtOW1hV3hsSUhCeWIycGxZM1JmZEdGbk9tVmthWFFnYldWa2FXRmZkR0ZuT21Wa2FYUWdkR0ZuY3pwbFpHbDBJRzFsWkdsaE9uSmxZV1FpTENKbGJXRnBiRjkyWlhKcFptbGxaQ0k2Wm1Gc2MyVXNJbU5zYVdWdWRFaHZjM1FpT2lJME15NHlORFV1T0RZdU1UTWlMQ0pqYkdsbGJuUkpaQ0k2SW5SaGMydHRZVzVoWjJWeUxtUmxkaUlzSW5CeVpXWmxjbkpsWkY5MWMyVnlibUZ0WlNJNkluTmxjblpwWTJVdFlXTmpiM1Z1ZEMxMFlYTnJiV0Z1WVdkbGNpNWtaWFlpTENKamJHbGxiblJCWkdSeVpYTnpJam9pTkRNdU1qUTFMamcyTGpFekluMC5mQXZjWWhVclNSemw4dWNfNV9qWlRSbDRieUJGOElhQnd4NEpWNl92dF9ObmlLelluNXpqcERWeUlOYW5jVnJaV3lTTlh0eUhycXNXeFlTRWFCWWdfcGx5RVI2RDJsUXlueXVESXdianFrTm56c1BXMmxkd3hHXzdjM0EtU3AtX3BLNHE5alJiYkhpTUNaSnhNNVdOWEU5QzJVZmZXUTRKd3hBN0JzaDZ6QlhjYUdpTDdFZmRTeDFPNzdqdGE4SUo5N3dXMHdTejdXUWtsZXFnYWFmX2lnQlo1QlhrZkdKaWlXNkFSYnRwdElUaUpZajhaZTZzQjlvNnlYUDBONnJpU1RtbXBBQ2NaYzg4WXJKaFBEYVpDSU56ODFkT01ZdW9rTmt6Y3ZzRXUxWUVHbk9sUHBUS3J5eERuR3FKSUVLVTJ5MkJ2RDFNYWhQWGZoMUpKb2h0MVEiLCJpZGVudGl0eSI6Im9saXNwMSszOXZuanBvbHBsc2tzQHNoYXJrbGFzZXJzLmNvbSJ9.Ab8VVYs63iuGlenhuNc38WCiQBqFDdeTGXE1CsLvtgQ" -H  "x-token-type: Access" -H  "Content-Type: application/x-www-form-urlencoded" -d "tag=application_name&value=taskmanager"
```

##### Response

```json
{
  "success": true,
  "message": "",
  "status_code": 200,
  "metadata": {},
  "data": {
    "id": 1415,
    "created_by": {
      "id": 89,
      "name": "Task Manager"
    },
    "updated_by": {
      "id": null,
      "name": null
    },
    "created_at": "2021-06-03 12:11:18.165276",
    "updated_at": null,
    "tag_detail": {
      "tag_id": 116,
      "tag_name": "application_name",
      "value": "taskmanager"
    },
    "project_detail": {
      "project_id": "1662",
      "project_name": "x1212xyz"
    }
  }
}
```

# 4. upload attachment in our project

## STEP 1: GET presigned url from API

```python
headers = {
    "Authorization": "<TOKEN>",
    "x-token-type":"Access",
}
params = ["<UUID_1>.<EXTENSION>", "<UUID_2>.<EXTENSION>", ...]  # array. this should be equal to no. of files we are uploading
```

### ENDPOINT

```shell
POST "/api/signed-url?parent_slug={PROJECT_SLUG}"
```

### EXAMPLE

#### CURL

```shell
curl -X POST "http://dataroom.local/api/signed-url?parent_slug=x215_d52d04ec-4fdc-4525-9ad1-3f196ebdbe2c" -H  "accept: application/json" -H  "Authorization: eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJvcmlnaW5hbF90b2tlbiI6ImV5SmhiR2NpT2lKU1V6STFOaUlzSW5SNWNDSWdPaUFpU2xkVUlpd2lhMmxrSWlBNklDSmllWFptWkZVMlRrTXpkbkIyWVhOWmQxOXViWEpXTUdGVlZGbGZUMWt6VUdsS1JVc3lla1pqVnkxWkluMC5leUpsZUhBaU9qRTJNakkzTXpBek1EQXNJbWxoZENJNk1UWXlNalk1TkRNd01Dd2lhblJwSWpvaU56Sm1NRGMzTWpNdFl6STBNUzAwTW1VNExXSTVaRGt0Tm1NMk5tTm1aV1F6TVdaaElpd2lhWE56SWpvaWFIUjBjSE02THk5a2IyTjJZWFZzZEM1amNtVmpaVzUwY21sakxtTnZiUzloZFhSb0wzSmxZV3h0Y3k5a2IyTjJZWFZzZENJc0ltRjFaQ0k2V3lKamNtVmpaVzUwY21sakxUWXdPV0l4WVdVM0xUQXlOVGd0TkRReE5pMDVNbUkxTFdFM09UWTVZbVUyWWpjMk9TSXNJbUZqWTI5MWJuUWlYU3dpYzNWaUlqb2lObVZpWmpkbVptWXROelF4WWkwME5UTTFMV0UwWmpRdE16UmhZak5tWlRGak56TTVJaXdpZEhsd0lqb2lRbVZoY21WeUlpd2lZWHB3SWpvaWRHRnphMjFoYm1GblpYSXVaR1YySWl3aVlXTnlJam9pTVNJc0ltRnNiRzkzWldRdGIzSnBaMmx1Y3lJNld5Sm9kSFJ3T2k4dmRHRnphMjFoYm1GblpYSXVaR1YySWwwc0luSmxZV3h0WDJGalkyVnpjeUk2ZXlKeWIyeGxjeUk2V3lKdlptWnNhVzVsWDJGalkyVnpjeUlzSW5WdFlWOWhkWFJvYjNKcGVtRjBhVzl1SWwxOUxDSnlaWE52ZFhKalpWOWhZMk5sYzNNaU9uc2lZV05qYjNWdWRDSTZleUp5YjJ4bGN5STZXeUp0WVc1aFoyVXRZV05qYjNWdWRDSXNJbTFoYm1GblpTMWhZMk52ZFc1MExXeHBibXR6SWl3aWRtbGxkeTF3Y205bWFXeGxJbDE5ZlN3aWMyTnZjR1VpT2lKdFpXUnBZVjkwWVdjNlpHVnNaWFJsSUcxbFpHbGhYM1JoWnpweVpXRmtJR1Z0WVdsc0lIQnliMnBsWTNRNmNtVmhaQ0J3Y205cVpXTjBYM1JoWnpwa1pXeGxkR1VnZEdGbmN6cHlaV0ZrSUhCeWIycGxZM1JmZEdGbk9uSmxZV1FnY0hKdmFtVmpkRHAzY21sMFpTQjBZV2R6T25keWFYUmxJRzFsWkdsaFgzUmhaenAzY21sMFpTQnRaV1JwWVRwM2NtbDBaU0J3Y205cVpXTjBYM1JoWnpwM2NtbDBaU0IxYzJWeU9uZHlhWFJsSUhWelpYSTZjbVZoWkNCd2NtOW1hV3hsSUhCeWIycGxZM1JmZEdGbk9tVmthWFFnYldWa2FXRmZkR0ZuT21Wa2FYUWdkR0ZuY3pwbFpHbDBJRzFsWkdsaE9uSmxZV1FpTENKbGJXRnBiRjkyWlhKcFptbGxaQ0k2Wm1Gc2MyVXNJbU5zYVdWdWRFaHZjM1FpT2lJME15NHlORFV1T0RZdU1UTWlMQ0pqYkdsbGJuUkpaQ0k2SW5SaGMydHRZVzVoWjJWeUxtUmxkaUlzSW5CeVpXWmxjbkpsWkY5MWMyVnlibUZ0WlNJNkluTmxjblpwWTJVdFlXTmpiM1Z1ZEMxMFlYTnJiV0Z1WVdkbGNpNWtaWFlpTENKamJHbGxiblJCWkdSeVpYTnpJam9pTkRNdU1qUTFMamcyTGpFekluMC5mQXZjWWhVclNSemw4dWNfNV9qWlRSbDRieUJGOElhQnd4NEpWNl92dF9ObmlLelluNXpqcERWeUlOYW5jVnJaV3lTTlh0eUhycXNXeFlTRWFCWWdfcGx5RVI2RDJsUXlueXVESXdianFrTm56c1BXMmxkd3hHXzdjM0EtU3AtX3BLNHE5alJiYkhpTUNaSnhNNVdOWEU5QzJVZmZXUTRKd3hBN0JzaDZ6QlhjYUdpTDdFZmRTeDFPNzdqdGE4SUo5N3dXMHdTejdXUWtsZXFnYWFmX2lnQlo1QlhrZkdKaWlXNkFSYnRwdElUaUpZajhaZTZzQjlvNnlYUDBONnJpU1RtbXBBQ2NaYzg4WXJKaFBEYVpDSU56ODFkT01ZdW9rTmt6Y3ZzRXUxWUVHbk9sUHBUS3J5eERuR3FKSUVLVTJ5MkJ2RDFNYWhQWGZoMUpKb2h0MVEiLCJpZGVudGl0eSI6Im9saXNwMSszOXZuanBvbHBsc2tzQHNoYXJrbGFzZXJzLmNvbSJ9.Ab8VVYs63iuGlenhuNc38WCiQBqFDdeTGXE1CsLvtgQ" -H  "x-token-type: Access" -H  "Content-Type: application/x-www-form-urlencoded" -d "["d05fee62-a24e-46d7-7eee-6a50908dd760.csv"]"
```

#### Response

```json
{
  "success": true,
  "message": "",
  "status_code": 200,
  "metadata": {},
  "data": [
    {
      "url": "https://crecentric-docvault.s3.amazonaws.com/",
      "fields": {
        "acl": "private",
        "key": "test/d05fee62-a24e-46d7-7eee-6a50908dd760.csv",
        "x-amz-algorithm": "AWS4-HMAC-SHA256",
        "x-amz-credential": "AKIAJGD4JRMAPC7K5K2Q/20210603/eu-west-2/s3/aws4_request",
        "x-amz-date": "20210603T072946Z",
        "policy": "eyJleHBpcmF0aW9uIjogIjIwMjEtMDYtMDNUMDg6Mjk6NDZaIiwgImNvbmRpdGlvbnMiOiBbeyJhY2wiOiAicHJpdmF0ZSJ9LCB7ImJ1Y2tldCI6ICJjcmVjZW50cmljLWRvY3ZhdWx0In0sIHsia2V5IjogInRlc3QvZDA1ZmVlNjItYTI0ZS00NmQ3LThlZWUtNmE1MDkwOGRkNzYwLmNzdiJ9LCB7IngtYW16LWFsZ29yaXRobSI6ICJBV1M0LUhNQUMtU0hBMjU2In0sIHsieC1hbXotY3JlZGVudGlhbCI6ICJBS0lBSkdENEpSTUFQQzdLNUsyUS8yMDIxMDYwMy9ldS13ZXN0LTIvczMvYXdzNF9yZXF1ZXN0In0sIHsieC1hbXotZGF0ZSI6ICIyMDIxMDYwM1QwNzI5NDZaIn1dfQ==",
        "x-amz-signature": "91942a3565475c1cf875aea48af1b02c16c9387191b9b5ece415fe5af23283f4"
      }
    }
  ]
}
```

## STEP 2: Use response obtained from STEP 1 to upload attachments to s3 bucket

```python
# Use VUE JS component here
```

## STEP 3: Record media information to database

```python
headers = {
    "Authorization": "<TOKEN>",
    "x-token-type":"Access",
}
params = {
    "parent_slug":"<PARENT_SLUG>", # slug of project obtained from 3. eg. response.data[0].slug 
    "reference":"media",
    "type":"file", 
    "file_name":"<FILE_NAME>", # actual file's name that is being uploaded
    "content_type":"<FILE_MIME_TYPE>",  # Valid MIME type. For more info : https://developer.mozilla.org/en-US/docs/Web/HTTP/Basics_of_HTTP/MIME_types/Common_types
    "key":"<AWS_KEY>", # <key> obtained from STEP 1 eg. response.data[0].fields.key
    "tags":"<KEY_VALUE_PAIR>", # object. Valid key: ["deal_uuid", "task_uuid", "log_uuid"]. eg. {"deal_uuid":"cc7c3f4-c2ca-11eb-8529-0242ac130003", "task_uuid":"cc5fc3f4-c2ca-11eb-8529-0242ac130003", "log_uuid":"cc7c3f4-c2ca-11eb-8529-0242ac130003"}
}
```

### ENDPOINT

```shell
POST "/api/media"
```

### EXAMPLE

#### CURL

```shell
curl -X POST "http://dataroom.local/api/media" -H  "accept: application/json" -H  "Authorization: Bearer eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJwYXlsb2FkIjp7ImlkIjoxOCwiZW1haWwiOiJhYmNAZ21haWwuY29tIiwibmFtZSI6IkFCQyB1c2VyIiwic3RhdHVzIjoiYWN0aXZlIiwib3RwIjpmYWxzZSwicHJlZmVyZW5jZSI6eyJ2aWV3IjoibGlzdCIsIm1lbnVfb3BlbiI6dHJ1ZX0sImltYWdlIjpudWxsLCJyb2xlIjp7ImlkIjozMywibmFtZSI6ImFwcGxpY2F0aW9uX2RldmVsb3BlciIsInNsdWciOiJhcHBsaWNhdGlvbl9kZXZlbG9wZXIifSwiY3JlYXRlZF9hdCI6IjIwMjAtMDItMDMgMDk6MjU6MDMuODM3OTc1KzA1OjQ1IiwidXBkYXRlZF9hdCI6IjIwMjAtMDItMDMgMDk6MjU6MDMuODM3OTg4KzA1OjQ1IiwibG9naW5fdGltZSI6IjIwMjEtMDYtMDIgMTQ6MzY6MzEuNTAzNTUxIn19.2OiCPQAOHBdLxZYxrdDR1vT4O0HJfzXwmmKVtL-PwvY" -H  "Content-Type: application/x-www-form-urlencoded" -d "parent_slug=123_681ef55d-a704-461e-a2ee-7d508e109bbf&reference=media&type=file&file_name=test.txt&content_type=application%2Fjson&key=x123&tags=%7B%22task_uuid%22%3A%22cc5fc3f4-c2ca-11eb-8529-0242ac130003%22%7D"
```

#### Response

```json
{
  "success": true,
  "message": "",
  "status_code": 200,
  "metadata": {},
  "data": {
    "id": 24158,
    "name": "test.txt",
    "slug": "testtxt_4c70dedd-0164-40a7-a8eb-e01526a93fe7",
    "type": "file",
    "description": "test.txt",
    "reference": {
      "reference_object": "projects",
      "reference_slug": "123_681ef55d-a704-461e-a2ee-7d508e109bbf",
      "reference_id": 1587
    },
    "path": "/storage/x123",
    "proxy_path": null,
    "text_extraction_pending": true,
    "collection": "123_681ef55d-a704-461e-a2ee-7d508e109bbf",
    "metadata": {
      "size": null,
      "content_type": "application/json"
    },
    "created_at": "2021-06-02 15:33:39.026729",
    "tag_detail": {
      "application_name": "taskmanager",
      "task_uuid": "cc5fc3f4-c2ca-11eb-8529-0242ac130003"
    }
  }
}
```

# 5. get an media/attachment

## STEP 1: SEARCH media/attachment information using tags

```python
headers = {
    "Authorization": "<TOKEN>",
    "x-token-type":"Access",
}
params = {
    "search_in":"tags",
    "exact_tags":"<ARRAY_OF_KEYS>" # eg. deal_uuid,task_uuid,log_uuid
    "tags":"<KEY_VALUE_PAIR>", # eg. {"deal_uuid":"cc5fc3f4-c2ca-11eb-8529-0242ac130003","task_uuid":"cc5fc3f4-c2ca-11eb-8529-0242ac130003","log_uuid":"cc7c3f4-c2ca-11eb-8529-0242ac130003"}
}
```

**Note: If exact_tags is empty then, default tag's key will be the keys provided in {tags} parameters. These keys will perform as OR operation.**

#### Example:

**1) If our parameters is as follows: (exact_tags is empty)**

```json
params = {
    "search_in":"tags",
    "tags":{"task_uuid":"cc5fc3f4-c2ca-11eb-8529-0242ac130003","log_uuid":"cc7c3f4-c2ca-11eb-8529-0242ac130003"}
}
```

**Output: We will get all media with task_uuid=cc5fc3f4-c2ca-11eb-8529-0242ac130003 OR log_uuid=cc7c3f4-c2ca-11eb-8529-0242ac130003**

#### Example:

**2) If our parameters is as follows: (exact_tags is not empty)**

```json
params = {
    "search_in":"tags",
    "exact_tags":"deal_uuid,task_uuid"
    "tags":{"task_uuid":"cc5fc3f4-c2ca-11eb-8529-0242ac130003","deal_uuid":"cc7c3f4-c2ca-11eb-8529-0242ac130003"}
}
```

**Output: We will get all media with task_uuid=cc5fc3f4-c2ca-11eb-8529-0242ac130003 AND deal_uuid=cc7c3f4-c2ca-11eb-8529-0242ac130003 that contains keys as deal_uuid and task_uuid only. All media which contains tags other than task_uuid and deal_uuid will be rejected.**

### ENDPOINT

```shell
POST "/api/search"
```

### EXAMPLE

#### CURL

```shell
curl -X POST "http://dataroom.local/api/search?page=1" -H  "accept: application/json" -H  "Authorization: eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJvcmlnaW5hbF90b2tlbiI6ImV5SmhiR2NpT2lKU1V6STFOaUlzSW5SNWNDSWdPaUFpU2xkVUlpd2lhMmxrSWlBNklDSmllWFptWkZVMlRrTXpkbkIyWVhOWmQxOXViWEpXTUdGVlZGbGZUMWt6VUdsS1JVc3lla1pqVnkxWkluMC5leUpsZUhBaU9qRTJNakkzTXpBek1EQXNJbWxoZENJNk1UWXlNalk1TkRNd01Dd2lhblJwSWpvaU56Sm1NRGMzTWpNdFl6STBNUzAwTW1VNExXSTVaRGt0Tm1NMk5tTm1aV1F6TVdaaElpd2lhWE56SWpvaWFIUjBjSE02THk5a2IyTjJZWFZzZEM1amNtVmpaVzUwY21sakxtTnZiUzloZFhSb0wzSmxZV3h0Y3k5a2IyTjJZWFZzZENJc0ltRjFaQ0k2V3lKamNtVmpaVzUwY21sakxUWXdPV0l4WVdVM0xUQXlOVGd0TkRReE5pMDVNbUkxTFdFM09UWTVZbVUyWWpjMk9TSXNJbUZqWTI5MWJuUWlYU3dpYzNWaUlqb2lObVZpWmpkbVptWXROelF4WWkwME5UTTFMV0UwWmpRdE16UmhZak5tWlRGak56TTVJaXdpZEhsd0lqb2lRbVZoY21WeUlpd2lZWHB3SWpvaWRHRnphMjFoYm1GblpYSXVaR1YySWl3aVlXTnlJam9pTVNJc0ltRnNiRzkzWldRdGIzSnBaMmx1Y3lJNld5Sm9kSFJ3T2k4dmRHRnphMjFoYm1GblpYSXVaR1YySWwwc0luSmxZV3h0WDJGalkyVnpjeUk2ZXlKeWIyeGxjeUk2V3lKdlptWnNhVzVsWDJGalkyVnpjeUlzSW5WdFlWOWhkWFJvYjNKcGVtRjBhVzl1SWwxOUxDSnlaWE52ZFhKalpWOWhZMk5sYzNNaU9uc2lZV05qYjNWdWRDSTZleUp5YjJ4bGN5STZXeUp0WVc1aFoyVXRZV05qYjNWdWRDSXNJbTFoYm1GblpTMWhZMk52ZFc1MExXeHBibXR6SWl3aWRtbGxkeTF3Y205bWFXeGxJbDE5ZlN3aWMyTnZjR1VpT2lKdFpXUnBZVjkwWVdjNlpHVnNaWFJsSUcxbFpHbGhYM1JoWnpweVpXRmtJR1Z0WVdsc0lIQnliMnBsWTNRNmNtVmhaQ0J3Y205cVpXTjBYM1JoWnpwa1pXeGxkR1VnZEdGbmN6cHlaV0ZrSUhCeWIycGxZM1JmZEdGbk9uSmxZV1FnY0hKdmFtVmpkRHAzY21sMFpTQjBZV2R6T25keWFYUmxJRzFsWkdsaFgzUmhaenAzY21sMFpTQnRaV1JwWVRwM2NtbDBaU0J3Y205cVpXTjBYM1JoWnpwM2NtbDBaU0IxYzJWeU9uZHlhWFJsSUhWelpYSTZjbVZoWkNCd2NtOW1hV3hsSUhCeWIycGxZM1JmZEdGbk9tVmthWFFnYldWa2FXRmZkR0ZuT21Wa2FYUWdkR0ZuY3pwbFpHbDBJRzFsWkdsaE9uSmxZV1FpTENKbGJXRnBiRjkyWlhKcFptbGxaQ0k2Wm1Gc2MyVXNJbU5zYVdWdWRFaHZjM1FpT2lJME15NHlORFV1T0RZdU1UTWlMQ0pqYkdsbGJuUkpaQ0k2SW5SaGMydHRZVzVoWjJWeUxtUmxkaUlzSW5CeVpXWmxjbkpsWkY5MWMyVnlibUZ0WlNJNkluTmxjblpwWTJVdFlXTmpiM1Z1ZEMxMFlYTnJiV0Z1WVdkbGNpNWtaWFlpTENKamJHbGxiblJCWkdSeVpYTnpJam9pTkRNdU1qUTFMamcyTGpFekluMC5mQXZjWWhVclNSemw4dWNfNV9qWlRSbDRieUJGOElhQnd4NEpWNl92dF9ObmlLelluNXpqcERWeUlOYW5jVnJaV3lTTlh0eUhycXNXeFlTRWFCWWdfcGx5RVI2RDJsUXlueXVESXdianFrTm56c1BXMmxkd3hHXzdjM0EtU3AtX3BLNHE5alJiYkhpTUNaSnhNNVdOWEU5QzJVZmZXUTRKd3hBN0JzaDZ6QlhjYUdpTDdFZmRTeDFPNzdqdGE4SUo5N3dXMHdTejdXUWtsZXFnYWFmX2lnQlo1QlhrZkdKaWlXNkFSYnRwdElUaUpZajhaZTZzQjlvNnlYUDBONnJpU1RtbXBBQ2NaYzg4WXJKaFBEYVpDSU56ODFkT01ZdW9rTmt6Y3ZzRXUxWUVHbk9sUHBUS3J5eERuR3FKSUVLVTJ5MkJ2RDFNYWhQWGZoMUpKb2h0MVEiLCJpZGVudGl0eSI6Im9saXNwMSszOXZuanBvbHBsc2tzQHNoYXJrbGFzZXJzLmNvbSJ9.Ab8VVYs63iuGlenhuNc38WCiQBqFDdeTGXE1CsLvtgQ" -H  "x-token-type: Access" -H  "Content-Type: application/x-www-form-urlencoded" -d "search_in=tags&tags=%7B%22task_uuid%22%3A%22cc5fc3f4-c2ca-11eb-8529-0242ac130003%22%7D"
```

#### Response

```json
{
  "success": true,
  "message": "",
  "status_code": 200,
  "metadata": {
    "per_page": 10,
    "total_records": 4,
    "current_page": 1,
    "total_page": 1
  },
  "data": [
    {
      "id": 24154,
      "name": "x123.json",
      "slug": "x123json_ca81f2f7-349b-4504-8878-3d1d190ec93f",
      "type": "file",
      "description": "x123.json",
      "path": "/storage/123x",
      "proxy_path": null,
      "text_extraction_pending": true,
      "collection": "x99_a0f597ee-020f-4e43-8441-6d0c816af136",
      "metadata": {
        "size": "1010",
        "content_type": "application/json"
      },
      "reference": {
        "reference_id": 1614,
        "reference_slug": "x99_a0f597ee-020f-4e43-8441-6d0c816af136",
        "reference_object": "projects"
      },
      "created_at": "2021-06-02 11:03:29.885882+05:45",
      "owner": "Task Manager",
      "owner_id": 88,
      "shared": false
    },
  ]
}
```

## STEP 2: View the attachment from s3 bucket using docvault

### ENDPOINT

```shell
GET "/api/{PATH}" # PATH is obtained from STEP 1: eg. data[0].path
```

### EXAMPLE

#### CURL

```shell
curl -X GET "https://docvault.crecentric.com/storage/d9a4e99c-d08c-40b4-9d1a-5349d3cb1edd.pdf"
```

#### Response

```python
# <BINARY DATA>
```

**************************
# List all child document of projects

- Lists all child documents(media/project) of project.


### ENDPOINT

```shell
GET "/api/projects/{PROJECT_ID}/childs?page=1" 
```

### EXAMPLE

#### CURL

```shell
curl -X GET "https://docvault.crecentric.com/api/projects/604/childs?page=1"
```

#### Response

```json
{
    "success": true,
    "message": "",
    "status_code": 200,
    "metadata": {
        "per_page": 10,
        "total_records": 5,
        "current_page": 1,
        "total_page": 1
    },
    "data": [
        {
            "id": 605,
            "name": "sub 31",
            "slug": "sub_31_fcbecf72-496e-453c-bc6e-dde2540fbc0c",
            "description": null,
            "path": null,
            "proxy_path": null,
            "text_extraction_pending": null,
            "type": "project",
            "metadata": null,
            "reference": {
                "reference_id": "",
                "reference_slug": "",
                "reference_object": ""
            },
            "created_at": "2021-06-25 09:31:37.526086+05:45",
            "owner": "Niroj Adhikary",
            "owner_id": 2,
            "permission": {
                "read": 1,
                "write": 1,
                "edit": 1,
                "delete": 1,
                "manage": 1
            },
            "shared": false,
            "starred": false,
            "file_count": 1,
            "folder_count": 0,
            "project_count": 0,
            "tag_detail": {}
        },
        {
            "id": 6415,
            "name": "hello",
            "slug": "hello_0683ac24-7aa1-4e5e-90b0-582ce2e26a8b",
            "description": "hello",
            "path": null,
            "proxy_path": null,
            "text_extraction_pending": false,
            "type": "folder",
            "metadata": null,
            "reference": {
                "reference_id": 604,
                "reference_slug": "9:31_e08600c8-dff7-4d0e-ac3d-eeb6bdc373db",
                "reference_object": "projects"
            },
            "created_at": "2021-06-25 09:31:41.281170+05:45",
            "owner": "Niroj Adhikary",
            "owner_id": 2,
            "permission": {
                "read": 1,
                "write": 1,
                "edit": 1,
                "delete": 1,
                "manage": 1
            },
            "shared": false,
            "starred": false,
            "file_count": 0,
            "folder_count": 0,
            "project_count": 0,
            "tag_detail": {}
        }
    ]
}
```

# List all child documents of folder

- Lists all child documents(media/project) of folder.


### ENDPOINT

```shell
GET "/api/media/{MEDIA_ID}/childs?page=1 " 
```

### EXAMPLE

#### CURL

```shell
curl -X GET "https://docvault.crecentric.com/api/media/7991/childs?page=1 "
```

#### Response

```json
{
  "success": true,
  "message": "",
  "status_code": 200,
  "metadata": {
    "per_page": 10,
    "total_records": 1,
    "current_page": 1,
    "total_page": 1
  },
  "data": [
    {
      "id": 8008,
      "name": "swagger (12).json",
      "slug": "swagger_12json_ff620336-0b91-419c-b4dd-83a63e9e0398",
      "description": "swagger (12).json",
      "path": "/storage/3072e76f-e9fd-47bf-805f-ed54b15c7595.json",
      "proxy_path": "/storage/3072e76f-e9fd-47bf-805f-ed54b15c7595.pdf",
      "text_extraction_pending": false,
      "type": "file",
      "reference": {
        "reference_id": 7991,
        "reference_slug": "f9_765941f9-3b90-447c-830d-e2b99139cd32",
        "reference_object": "media"
      },
      "metadata": {
        "size": "233163",
        "content_type": "application/json"
      },
      "created_at": "2021-08-04 11:22:46.849829+05:45",
      "owner": "Ramesh Pradhan",
      "owner_id": 67,
      "permission": {
        "read": 1,
        "write": 1,
        "edit": 1,
        "delete": 1,
        "manage": 1
      },
      "shared": false,
      "starred": false,
      "file_count": null,
      "folder_count": null,
      "project_count": null,
      "tags_detail": {}
    }
  ]
}
```

**************************
# List all projects shared to me and all projects that is created by me.

### Option 1

- Lists all projects shared to me and all projects that is created by me.

**Note: If you are super-admin with manage permission then you will be listed with all the projects created by others too.**

### ENDPOINT

```shell
GET "/api/projects/all-listing?page=1" 
```

### EXAMPLE

#### CURL

```shell
curl -X GET "https://docvault.crecentric.com/api/projects/all-listing?page=1"
```

#### Response

```json
{
  "success": true,
  "message": "",
  "status_code": 200,
  "metadata": {
    "per_page": 10,
    "total_records": 144,
    "current_page": 1,
    "total_page": 15
  },
  "data": [
    {
      "id": 742,
      "name": "ramesh project 123",
      "slug": "ramesh_project_123_1ff6890c-4fc5-4f31-8b8d-6062330acba5",
      "reference": {
        "reference_id": "",
        "reference_slug": "",
        "reference_object": ""
      },
      "parent_id": null,
      "created_at": "2021-08-02 10:10:23.097187+05:45",
      "owner": "Ramesh Pradhan",
      "owner_id": 67,
      "permission": {
        "read": 1,
        "write": 1,
        "edit": 1,
        "delete": 1,
        "manage": 1
      },
      "shared": false,
      "starred": false,
      "breadcrumbs": [
        {
          "id": 742,
          "name": "ramesh project 123",
          "type": "project"
        }
      ],
      "tags_detail": {}
    },
    {
      "id": 741,
      "name": "project 29",
      "slug": "project_29_a151b58b-81a6-4fe4-be1a-b1a4aaa47ff1",
      "reference": {
        "reference_id": "",
        "reference_slug": "",
        "reference_object": ""
      },
      "parent_id": null,
      "created_at": "2021-07-29 09:36:33.151625+05:45",
      "owner": "Task Manager",
      "owner_id": 69,
      "permission": {
        "read": 1,
        "write": 1,
        "edit": 1,
        "delete": 1,
        "manage": 1
      },
      "shared": false,
      "starred": false,
      "breadcrumbs": [
        {
          "id": 741,
          "name": "project 29",
          "type": "project"
        }
      ],
      "tags_detail": {
        "application_name": "taskmanager",
        "deal_uuid": "995f5e06-79e2-430a-b961-1dce8edad1f5"
      }
    }
  ]
}
```

### Option 2

- Lists all projects shared to me and all projects that is created by me.

**Note: If you are super-admin with manage permission then you will be listed with all the projects created by others too.**

### ENDPOINT

```shell
POST "/api/projects/search" 
```

### EXAMPLE

#### CURL

```shell
curl -X POST "https://docvault.crecentric.com/api/projects/search"
```

#### Response

```json
{
    "success": true,
    "message": "",
    "status_code": 200,
    "metadata": {
        "per_page": 10,
        "total_records": 144,
        "current_page": 1,
        "total_page": 15
    },
    "data": [
        {
            "id": 742,
            "name": "ramesh project 123",
            "slug": "ramesh_project_123_1ff6890c-4fc5-4f31-8b8d-6062330acba5",
            "reference": {
                "reference_id": "",
                "reference_slug": "",
                "reference_object": ""
            },
            "parent_id": null,
            "created_at": "2021-08-02 10:10:23.097187+05:45",
            "owner": "Ramesh Pradhan",
            "owner_id": 67,
            "permission": {
                "read": 1,
                "write": 1,
                "edit": 1,
                "delete": 1,
                "manage": 1
            },
            "shared": false,
            "starred": false,
            "breadcrumbs": [
                {
                    "id": 742,
                    "name": "ramesh project 123",
                    "type": "project"
                }
            ],
            "tags_detail": {}
        },
        {
            "id": 741,
            "name": "project 29",
            "slug": "project_29_a151b58b-81a6-4fe4-be1a-b1a4aaa47ff1",
            "reference": {
                "reference_id": "",
                "reference_slug": "",
                "reference_object": ""
            },
            "parent_id": null,
            "created_at": "2021-07-29 09:36:33.151625+05:45",
            "owner": "Task Manager",
            "owner_id": 69,
            "permission": {
                "read": 1,
                "write": 1,
                "edit": 1,
                "delete": 1,
                "manage": 1
            },
            "shared": false,
            "starred": false,
            "breadcrumbs": [
                {
                    "id": 741,
                    "name": "project 29",
                    "type": "project"
                }
            ],
            "tags_detail": {
                "application_name": "taskmanager",
                "deal_uuid": "995f5e06-79e2-430a-b961-1dce8edad1f5"
            }
        }
    ]
}
```

**************************
__References__:
[Docvault Swagger Docs](https://docvault.crecentric.com/swagger/)
